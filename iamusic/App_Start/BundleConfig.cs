﻿using System.Web;
using System.Web.Optimization;

namespace iamusic
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/sb-admin.js",
                      "~/Scripts/sb-admin.min.js",
                      "~/Scripts/sb-admin-charts.js",
                      "~/Scripts/sb-admin-charts.min.js",
                      "~/Scripts/sb-admin-datatables.js",
                      "~/Scripts/sb-admin-datatables.min.js",
                      "~/Scripts/bootstrap.bundle.js",
                      "~/Scripts/bootstrap.bundle.min.js",
                      "~/Scripts/bootstrap.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/feed.css",
                      "~/Content/publi.css",
                      "~/Content/sb-admin.css",
                      "~/Content/sb-admin.min.css"));
        }
    }
}
