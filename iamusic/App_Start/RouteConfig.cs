﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace iamusic
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            //banda

            routes.MapRoute(
                name: "BandaIndex",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Banda", action = "Banda", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "BandaAdicionar",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Banda", action = "Adicionar", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "BandaEditar",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Banda", action = "Editar", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "BandaExcluir",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Banda", action = "Excluir", id = UrlParameter.Optional }
            );

            //agenda

            routes.MapRoute(
               name: "AgendaIndex",
               url: "{controller}/{action}/{id}",
               defaults: new { controller = "Agenda", action = "Agenda", id = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "AgendaAdicionar",
               url: "{controller}/{action}/{id}",
               defaults: new { controller = "Agenda", action = "Adicionar", id = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "AgendaExcluir",
               url: "{controller}/{action}/{id}",
               defaults: new { controller = "Agenda", action = "Excluir", id = UrlParameter.Optional }
           );


            //publicacao

            routes.MapRoute(
               name: "PublicacaoIndex",
               url: "{controller}/{action}/{id}",
               defaults: new { controller = "Publicacao", action = "Publicacao", id = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "PublicacaoAdicionar",
               url: "{controller}/{action}/{id}",
               defaults: new { controller = "Publicacao", action = "Adicionar", id = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "PublicacaoExcluir",
               url: "{controller}/{action}/{id}",
               defaults: new { controller = "Publicacao", action = "Excluir", id = UrlParameter.Optional }
           );


            //comentario

            routes.MapRoute(
                name: "ComentarioIndex",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Comentario", action = "Comentario", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ComentarioAdicionar",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Comentario", action = "Adicionar", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ComentarioEditar",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Comentario", action = "Editar", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ComentarioExcluir",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Comentario", action = "Excluir", id = UrlParameter.Optional }
            );
        }
    }
}
