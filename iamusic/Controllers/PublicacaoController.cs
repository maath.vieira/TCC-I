﻿using iamusic.Aplicacao;
using iamusic.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace iamusic.Controllers
{
    public class PublicacaoController : Controller
    {
        // GET: Publicacao
        public ActionResult Publicacao()
        {
            var appPublicacao = new PublicacaoAplicacao();
            var listaPublicacao = appPublicacao.ListarTodos();
            return View(listaPublicacao);
        }

        public ActionResult Adicionar()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Adicionar(Publicacao publicacao)
        {
            if (ModelState.IsValid)
            {
                var appPublicacao = new PublicacaoAplicacao();
                appPublicacao.Salvar(publicacao);
                return RedirectToAction("Publicacao");
            }
            return View(publicacao);
        }

        public ActionResult Excluir(int id)
        {
            var appPublicacao = new PublicacaoAplicacao();
            var publicacao = appPublicacao.ListarPorId(id);

            if (publicacao == null)
            {
                return HttpNotFound();
            }

            return View(publicacao);
        }

        [HttpPost, ActionName("Excluir")]
        public ActionResult ExcluirConfirmado(int id)
        {
            var appPublicacao = new PublicacaoAplicacao();
            appPublicacao.Excluir(id);
            return RedirectToAction("Publicacao");
        }

        public ActionResult Editar(int id)
        {
            var appPublicacao = new PublicacaoAplicacao();
            var publicacao = appPublicacao.ListarPorId(id);

            if (publicacao == null)
            {
                return HttpNotFound();
            }

            return View(publicacao);
        }

        [HttpPost]
        public ActionResult Editar(Publicacao publicacao)
        {
            if (ModelState.IsValid)
            {
                var appPublicacao = new PublicacaoAplicacao();
                appPublicacao.Salvar(publicacao);
                return RedirectToAction("Publicacao");
            }
            return View(publicacao);
        }
    }
}
