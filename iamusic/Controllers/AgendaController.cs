﻿using iamusic.Aplicacao;
using iamusic.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace iamusic.Controllers
{
    public class AgendaController : Controller
    {
        // GET: Agenda
        public ActionResult Agenda()
        {
            var appAgenda = new AgendaAplicacao();
            var listaAgenda = appAgenda.ListarTodos();
            return View(listaAgenda);
        }

        public ActionResult Adicionar()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Adicionar(Agenda agenda)
        {
            if (ModelState.IsValid)
            {
                var appAgenda = new AgendaAplicacao();
                appAgenda.Inserir(agenda);
                return RedirectToAction("Index");
            }
            return View(agenda);
        }

        public ActionResult Excluir(int id)
        {
            var appAgenda = new AgendaAplicacao();
            var agenda = appAgenda.ListarPorId(id);

            if (agenda == null)
            {
                return HttpNotFound();
            }

            return View(agenda);
        }

        [HttpPost, ActionName("Excluir")]
        public ActionResult ExcluirConfirmado(int id)
        {
            var appAgenda = new PublicacaoAplicacao();
            appAgenda.Excluir(id);
            return RedirectToAction("Agenda");
        }
    }
}