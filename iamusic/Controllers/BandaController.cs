﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using iamusic.Aplicacao;
using iamusic.Dominio;

namespace iamusic.Controllers
{
    public class BandaController : Controller
    {
        // GET: Banda
        public ActionResult Banda()
        {
            var appBanda = new BandaAplicacao();
            var listaBandas = appBanda.ListarTodos();
            return View(listaBandas);
        }

        public ActionResult Adicionar()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult Adicionar(Banda banda)
        {
            if (ModelState.IsValid)
            {
                var appBanda = new BandaAplicacao();
                appBanda.Salvar(banda);
                return RedirectToAction("Banda");
            }
            return View(banda);
        }

        public ActionResult Excluir(int id)
        {
            var appBanda = new BandaAplicacao();
            var banda = appBanda.ListarPorId(id);

            if (banda == null)
            {
                return HttpNotFound();
            }

            return View(banda);
        }

        [HttpPost, ActionName("Excluir")]
        public ActionResult ExcluirConfirmado(int id)
        {
                var appBanda = new BandaAplicacao();
                appBanda.Excluir(id);
                return RedirectToAction("Banda");
        }

        public ActionResult Editar(int id)
        {
            var appBanda = new BandaAplicacao();
            var banda = appBanda.ListarPorId(id);

            if (banda == null)
            {
                return HttpNotFound();
            }

            return View(banda);
        }

        [HttpPost]
        public ActionResult Editar(Banda banda)
        {
            if (ModelState.IsValid)
            {
                var appBanda = new BandaAplicacao();
                appBanda.Salvar(banda);
                return RedirectToAction("Banda");
            }
            return View(banda);
        }
    }
}