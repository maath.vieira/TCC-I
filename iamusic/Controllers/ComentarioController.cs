﻿using iamusic.Aplicacao;
using iamusic.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace iamusic.Controllers
{
    public class ComentarioController : Controller
    {
        // GET: Comentario
        public ActionResult Comentario()
        {
            var appComentario = new ComentarioAplicacao();
            var listaComentario = appComentario.ListarTodos();
            return View(listaComentario);
        }

        public ActionResult Adicionar()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Adicionar(Comentario comentario)
        {
            if (ModelState.IsValid)
            {
                var appComentario = new ComentarioAplicacao();
                appComentario.Salvar(comentario);
                return RedirectToAction("Comentario");
            }
            return View(comentario);
        }

        public ActionResult Excluir(int id)
        {
            var appComentario = new ComentarioAplicacao();
            var comentario = appComentario.ListarPorId(id);

            if (comentario == null)
            {
                return HttpNotFound();
            }

            return View(comentario);
        }

        [HttpPost, ActionName("Excluir")]
        public ActionResult ExcluirConfirmado(int id)
        {
            var appComentario = new ComentarioAplicacao();
            appComentario.Excluir(id);
            return RedirectToAction("Comentario");
        }

        public ActionResult Editar(int id)
        {
            var appComentario = new ComentarioAplicacao();
            var comentario = appComentario.ListarPorId(id);

            if (comentario == null)
            {
                return HttpNotFound();
            }

            return View(comentario);
        }

        [HttpPost]
        public ActionResult Editar(Comentario comentario)
        {
            if (ModelState.IsValid)
            {
                var appComentario = new ComentarioAplicacao();
                appComentario.Salvar(comentario);
                return RedirectToAction("Comentario");
            }
            return View(comentario);
        }
    }
}