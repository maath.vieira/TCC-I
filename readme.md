<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
</head>
<body>
<header>
	<h2 style="text-align: center;">Iamusic - Sistema para Músicos Independentes</h2>
	<h4 style="text-align: center;">O sistema tem intuíto de ajudar músicos a terem mais oportunidades de trabalho.</h5>
</header>
<article>
	<h3>Ferramentas e Tecnologias Utilizadas</h3>
	<ul>
		<li>Sql Serve 2017 - <a href="https://www.microsoft.com/pt-br/sql-server/sql-server-2017"> https://www.microsoft.com/pt-br/sql-server/sql-server-2017</a></li>
		</br>
		<li>IDE Microsoft Visual Studio - <a href="https://visualstudio.microsoft.com/pt-br/"> https://visualstudio.microsoft.com/pt-br/</a></li>
		</br>
		<li>ASP.NET MVC - <a href="https://www.asp.net/mvc"> https://www.asp.net/mvc</a></li>
		</br>
		<li>C# - <a href="https://docs.microsoft.com/pt-br/dotnet/csharp/"> https://docs.microsoft.com/pt-br/dotnet/csharp/</a></li>
		</br>
		<li>Android Studio - <a href="https://developer.android.com/studio/"> https://developer.android.com/studio/</a></li>
		</br>
		<li>Bootstrap - <a href="https://getbootstrap.com/"> https://getbootstrap.com/</a></li>
	</ul>
	<h3>Execução do Projeto</h3>
		<ol>
			<li>Clone o projeto.</li>
				<pre>git@gitlab.com:maath.vieira/TCC-I.wiki.git</pre>
				</br>
			<li>Importe para o Visual Studio.</li>
			</br>
			<li>Importe o script no sqlserver que está localizado na pasta base.</li>
			</br>
		</ol>
	<h3>Licensa</h3>
        <a href="https://gitlab.com/maath.vieira/TCC-I/blob/master/licensa.txt"> https://gitlab.com/maath.vieira/TCC-I/blob/master/licensa.txt</a>
	<h3>Autor</h3>
	<p>Matheus Delgado Vieira</p>
	
</article>

</body>
</html>