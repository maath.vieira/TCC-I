﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iamusic.Dominio
{
    

    public class Agenda
    {
        public int id { get; set; }
        [DisplayName("Banda")]
        public int idBanda { get; set; }
        [DisplayName("Genero")]
        public int idGenero { get; set; }
        [DisplayName("Estabelecimento")]
        public int idEstabelecimento { get; set; }
        
    }
}
