﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iamusic.Dominio
{
    public class Banda
    {
        public int id { get; set; }

        [DisplayName("Banda")]
        public string nomeBanda { get; set; }
        [DisplayName("Ativa")]
        public string ativa { get; set; }
        
    }
}
