﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iamusic.Dominio
{
    public class Publicacao
    {
        public int id { get; set; }
        [DisplayName("Nova Publicação")]
        public string descricao { get; set; }
        public string caminho { get; set; }
        [DisplayName("Banda")]
        public int idBanda { get; set; }
        [DisplayName("Estabelecimento")]
        public int idEstabelecimento { get; set; }
    }
}
