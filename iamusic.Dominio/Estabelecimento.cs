﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iamusic.Dominio
{
    public class Estabelecimento
    {
        public int id { get; set; }
        [DisplayName("Razão Social")]
        public string razaoSocial { get; set; }
        [DisplayName("CNPJ")]
        public string cnpj { get; set; }
        [DisplayName("RUA")]
        public string rua { get; set; }
        public int idUsuario { get; set; }
        public int idCidade { get; set; }
        public int idEstado { get; set; }
    }
}
