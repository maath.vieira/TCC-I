﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iamusic.Dominio
{
    public class Comentario
    {
        public int id { get; set; }

        [DisplayName("Novo Comentário")]
        public string descricao { get; set; }

        public int idBanda { get; set; }
        public int idEstabelecimento { get; set; }
    }
}
