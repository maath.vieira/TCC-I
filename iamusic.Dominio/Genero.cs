﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iamusic.Dominio
{
    public class Genero
    {
        public int id { get; set; }
        [DisplayName("Genero")]
        public string genero { get; set; }
    }
}
