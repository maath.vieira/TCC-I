﻿using iamusic.Dominio;
using iamusic.Repositorio;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iamusic.Aplicacao
{
    public class AgendaAplicacao
    {
        private bd bd;

        public List<Agenda> ListarTodos()
        {
            using(bd = new bd()) {
                var strQuery = " ";
                strQuery += "SELECT * FROM agenda ";
                /**strQuery += " (((agenda";
                strQuery += "INNER JOIN banda ON agenda.idBanda = banda.id)";
                strQuery += "INNER JOIN genero ON agenda.idGenero = genero.id)";
                strQuery += "INNER JOIN estabelecimento ON agenda.idEstabelecimento = estabelecimento.id)";**/
                var retorno = bd.ExecutaComandoComRetorno(strQuery);
                return ReaderEmLista(retorno);
            }
        }

        public Agenda ListarPorId(int id)
        {
            using (bd = new bd())
            {
                var strQuery = " ";
                strQuery += String.Format("SELECT * FROM agenda WHERE id = {0}", id);
                var retorno = bd.ExecutaComandoComRetorno(strQuery);
                return ReaderEmLista(retorno).FirstOrDefault();
            }
        }

        private List<Agenda>ReaderEmLista(SqlDataReader reader)
        {
            var agenda = new List<Agenda>();

            while (reader.Read())
            {
                var tempoObjeto = new Agenda()
                {
                    id = int.Parse(reader["id"].ToString()),
                    idBanda = int.Parse(reader["idBanda"].ToString()),
                    idGenero = int.Parse(reader["idGenero"].ToString()),
                    idEstabelecimento = int.Parse(reader["idEstabelecimento"].ToString())
                    
                };
                
                agenda.Add(tempoObjeto);
            }

            reader.Close();
            return agenda;
        }

        public void Inserir(Agenda agenda)
        {
            var strQuery = " ";
            strQuery += "INSERT INTO agenda (idBanda, idGenero, idEstabelecimento)";
            strQuery += string.Format(" VALUES ('{0}', '{1}', '{2}')", agenda.idBanda, agenda.idGenero, agenda.idEstabelecimento);

            using(bd = new bd())
            {
                bd.ExecutaComando(strQuery);
            }
        }
        
        public void Excluir(int id)
        {
            using (bd = new bd()) { 
                var strQuery = string.Format(" DELETE FROM agenda WHERE id = {0}", id);
                bd.ExecutaComando(strQuery);
            }
        }
    }
}
