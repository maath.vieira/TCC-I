﻿using iamusic.Dominio;
using iamusic.Repositorio;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iamusic.Aplicacao
{
    public class ComentarioAplicacao
    {
        private bd bd;

        private void Inserir(Comentario comentario)
        {
            var strQuery = " ";
            strQuery += "INSERT INTO comentario (descricao, idBanda, idEstabelecimento)";
            strQuery += string.Format(" VALUES ('{0}', '2', '7')", comentario.descricao);

            using (bd = new bd())
            {
                bd.ExecutaComando(strQuery);
            }
        }

        private void Alterar(Comentario comentario)
        {
            var strQuery = " ";
            strQuery += "UPDATE comentario SET ";
            strQuery += string.Format("descricao = '{0}' ", comentario.descricao);
            strQuery += string.Format("WHERE id = {0}", comentario.id);

            using (bd = new bd())
            {
                bd.ExecutaComando(strQuery);
            }
        }

        public void Salvar(Comentario comentario)
        {
            if (comentario.id > 0)
            {
                Alterar(comentario);
            }
            else
            {
                Inserir(comentario);
            }
        }

        public void Excluir(int id)
        {
            using (bd = new bd())
            {
                var strQuery = string.Format(" DELETE FROM comentario WHERE id = {0}", id);
                bd.ExecutaComando(strQuery);
            }
        }

        public List<Comentario> ListarTodos()
        {
            using (bd = new bd())
            {
                var strQuery = "SELECT id, descricao, idBanda, idEstabelecimento FROM comentario";
                var retorno = bd.ExecutaComandoComRetorno(strQuery);
                return ReaderEmLista(retorno);
            }
        }

        public Comentario ListarPorId(int id)
        {
            using (bd = new bd())
            {
                var strQuery = " ";
                strQuery += string.Format("SELECT * FROM comentario WHERE id = {0}", id);
                var retorno = bd.ExecutaComandoComRetorno(strQuery);
                return ReaderEmLista(retorno).FirstOrDefault();
            }
        }

        private List<Comentario> ReaderEmLista(SqlDataReader reader)
        {
            var comentarios = new List<Comentario>();
            while (reader.Read())
            {
                var tempoObjeto = new Comentario()
                {
                    id = int.Parse(reader["id"].ToString()),
                    descricao = reader["descricao"].ToString(),
                    idBanda = int.Parse(reader["idBanda"].ToString()),
                    idEstabelecimento = int.Parse(reader["idEstabelecimento"].ToString())
                };

                comentarios.Add(tempoObjeto);
            }

            reader.Close();
            return comentarios;

        }
    }
}
