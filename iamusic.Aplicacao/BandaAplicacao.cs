﻿using iamusic.Dominio;
using iamusic.Repositorio;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace iamusic.Aplicacao
{
    public class BandaAplicacao
    {
        private bd bd;

        private void Inserir(Banda banda)
        {
            var strQuery = " ";
            strQuery += "INSERT INTO banda(nomeBanda, ativa)";
            strQuery += string.Format(" VALUES ('{0}', '{1}')", banda.nomeBanda, banda.ativa);

            using(bd = new bd())
            {
                bd.ExecutaComando(strQuery);
            }
        }

        public Banda ListarPorId(int id)
        {
            using (bd = new bd())
            {
                var strQuery = " ";
                strQuery += string.Format("SELECT * FROM banda WHERE id = {0}", id);
                var retorno = bd.ExecutaComandoComRetorno(strQuery);
                return ReaderEmLista(retorno).FirstOrDefault();
            }
        }

        private void Alterar(Banda banda)
        {
            var strQuery = " ";
            strQuery += "UPDATE banda SET ";
            strQuery += string.Format("nomeBanda = '{0}', ", banda.nomeBanda);
            strQuery += string.Format("ativa = '{0}' ", banda.ativa);
            strQuery += string.Format("WHERE id = {0}", banda.id);

            using(bd = new bd())
            {
                bd.ExecutaComando(strQuery);
            }
        }

        public void Salvar(Banda banda)
        {
            if(banda.id > 0)
            {
                Alterar(banda);
            }
            else
            {
                Inserir(banda);
            }
        }

        public void Excluir(int id)
        {
            using (bd = new bd())
            {
                var strQuery = string.Format(" DELETE FROM banda WHERE id = {0}", id);
                bd.ExecutaComando(strQuery);
            }
        }

        public List<Banda> ListarTodos()
        {
            using (bd = new bd()) { 
                var strQuery = "SELECT * FROM banda WHERE ativa = 0";
                var retorno =  bd.ExecutaComandoComRetorno(strQuery);
                return ReaderEmLista(retorno);
            }
        }

        private List<Banda>ReaderEmLista(SqlDataReader reader)
        {
            var bandas = new List<Banda>();
            while (reader.Read())
            {
                var tempoObjeto = new Banda()
                {
                    id = int.Parse(reader["id"].ToString()),
                    nomeBanda = reader["nomeBanda"].ToString(),
                    ativa = reader["ativa"].ToString()
                };

                bandas.Add(tempoObjeto);
            }

            reader.Close();
            return bandas;

        }
    }
}
