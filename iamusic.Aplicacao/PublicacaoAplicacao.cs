﻿using iamusic.Dominio;
using iamusic.Repositorio;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iamusic.Aplicacao
{
    public class PublicacaoAplicacao
    {
        private bd bd;
        
        private void Inserir(Publicacao publicacao)
        {
            var strQuery = " ";
            strQuery += "INSERT INTO publicacao (descricao, caminho, idBanda, idEstabelecimento)";
            strQuery += string.Format(" VALUES ('{0}', 'null', '2', '7')", publicacao.descricao);

            using (bd = new bd())
            {
                bd.ExecutaComando(strQuery);
            }
        }

        private void Alterar(Publicacao publicacao)
        {
            var strQuery = " ";
            strQuery += "UPDATE publicacao SET ";
            strQuery += string.Format("descricao = '{0}' ", publicacao.descricao);
            strQuery += string.Format("WHERE id = {0}", publicacao.id);

            using (bd = new bd())
            {
                bd.ExecutaComando(strQuery);
            }
        }

        public void Salvar(Publicacao publicacao)
        {
            if (publicacao.id > 0)
            {
                Alterar(publicacao);
            }
            else
            {
                Inserir(publicacao);
            }
        }

        public void Excluir(int id)
        {
            using (bd = new bd())
            {
                var strQuery = string.Format(" DELETE FROM publicacao WHERE id = {0}", id);
                bd.ExecutaComando(strQuery);
            }
        }

        public List<Publicacao> ListarTodos()
        {
            using (bd = new bd())
            {
                var strQuery = "SELECT id, descricao, idBanda, idEstabelecimento FROM publicacao";
                var retorno = bd.ExecutaComandoComRetorno(strQuery);
                return ReaderEmLista(retorno);
            }
        }

        public Publicacao ListarPorId(int id)
        {
            using (bd = new bd())
            {
                var strQuery = " ";
                strQuery += string.Format("SELECT * FROM publicacao WHERE id = {0}", id);
                var retorno = bd.ExecutaComandoComRetorno(strQuery);
                return ReaderEmLista(retorno).FirstOrDefault();
            }
        }

        private List<Publicacao> ReaderEmLista(SqlDataReader reader)
        {
            var publicacoes = new List<Publicacao>();
            while (reader.Read())
            {
                var tempoObjeto = new Publicacao()
                {
                    id = int.Parse(reader["id"].ToString()),
                    descricao = reader["descricao"].ToString(),
                    idBanda = int.Parse(reader["idBanda"].ToString()),
                    idEstabelecimento = int.Parse(reader["idEstabelecimento"].ToString())
                };

                publicacoes.Add(tempoObjeto);
            }

            reader.Close();
            return publicacoes;

        }
    }
}
